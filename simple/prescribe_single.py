import os
import sys
import argparse
import numpy as np
from numpy.core.fromnumeric import nonzero
import pandas as pd
from datetime import timedelta
from scipy.ndimage import gaussian_filter1d

from covid_xprize.standard_predictor.xprize_predictor import XPrizePredictor
from covid_xprize.scoring.prescriptor_scoring import weight_prescriptions_by_cost
NUM_PRESCRIPTIONS = 10

IP_MAX_VALUES = {
    'C1_School closing': 3,
    'C2_Workplace closing': 3,
    'C3_Cancel public events': 2,
    'C4_Restrictions on gatherings': 4,
    'C5_Close public transport': 2,
    'C6_Stay at home requirements': 3,
    'C7_Restrictions on internal movement': 2,
    'C8_International travel controls': 4,
    'H1_Public information campaigns': 2,
    'H2_Testing policy': 3,
    'H3_Contact tracing': 2,
    'H6_Facial Coverings': 4
}

IP_COLS = list(IP_MAX_VALUES.keys())


class FastPredictor(XPrizePredictor):
    def get_last_known_date(self, geo_id):
        return self.df[self.df.GeoID == geo_id].Date.max()

    def predict_from_df(self,
                        start_date_str: str,
                        end_date_str: str,
                        npis_df: pd.DataFrame, future_df: pd.DataFrame) -> pd.DataFrame:
        start_date = pd.to_datetime(start_date_str, format='%Y-%m-%d')
        end_date = pd.to_datetime(end_date_str, format='%Y-%m-%d')
        nb_days = (end_date - start_date).days + 1

        # Prepare the output
        forecast = {"CountryName": [],
                    "RegionName": [],
                    "Date": [],
                    "PredictedDailyNewCases": []}

        # Fix for past predictions
        geos = npis_df.GeoID.unique()
        truncated_df = self.df[self.df.Date < start_date_str]
        if future_df is not None:
            #print(future_df[future_df.Date < start_date_str])
            #print(future_df[ (future_df.Date < start_date_str) & (future_df.Date > truncated_df.Date.max())] )
            truncated_df = pd.concat([truncated_df, future_df[(
                future_df.Date < start_date_str) & (future_df.Date > truncated_df.Date.max())]])
        country_samples = self._create_country_samples(
            truncated_df, geos, False)

        # For each requested geo
        geos = npis_df.GeoID.unique()
        for g in geos:
            cdf = truncated_df[truncated_df.GeoID == g]
            if len(cdf) == 0:
                # we don't have historical data for this geo: return zeroes
                pred_new_cases = [0] * nb_days
                geo_start_date = start_date
            else:
                last_known_date = cdf.Date.max()
                # Start predicting from start_date, unless there's a gap since last known date
                geo_start_date = min(
                    last_known_date + np.timedelta64(1, 'D'), start_date)
                npis_gdf = npis_df[(npis_df.Date >= geo_start_date) & (
                    npis_df.Date <= end_date)]

                pred_new_cases = self._get_new_cases_preds(
                    cdf, g, npis_gdf, country_samples)

            # Append forecast data to results to return
            country = npis_df[npis_df.GeoID == g].iloc[0].CountryName
            region = npis_df[npis_df.GeoID == g].iloc[0].RegionName
            for i, pred in enumerate(pred_new_cases):
                forecast["CountryName"].append(country)
                forecast["RegionName"].append(region)
                current_date = geo_start_date + pd.offsets.Day(i)
                forecast["Date"].append(current_date)
                forecast["PredictedDailyNewCases"].append(pred)

        forecast_df = pd.DataFrame.from_dict(forecast)
        # Return only the requested predictions
        return forecast_df[(forecast_df.Date >= start_date) & (forecast_df.Date <= end_date)]


def get_script_path():
    return os.path.dirname(os.path.realpath(sys.argv[0]))


def get_filtered_first_order_derivative(x):
    filtered_data = gaussian_filter1d(x, 1)
    # first order derivatives
    first_order_derivative = np.gradient(filtered_data, 1)
    return first_order_derivative


def add_geo_id(df: pd.DataFrame):
    df["GeoID"] = np.where(df["RegionName"].isnull(),
                           df["CountryName"],
                           df["CountryName"] + ' / ' + df["RegionName"])
    return df


def get_predicted_cases(start_date_str: str,
                        end_date_str: str,
                        npis_df: pd.DataFrame):

    predictor = XPrizePredictor()
    return predictor.predict_from_df(start_date_str, end_date_str, npis_df)


def get_cost_df(costs_file, npis_df):
    # Load IP cost weights
    cost_df = pd.read_csv(costs_file)

    # Only use costs of geos we've predicted for
    cost_df = cost_df[cost_df.CountryName.isin(npis_df.CountryName) &
                      cost_df.RegionName.isin(npis_df.RegionName)]
    cost_df = add_geo_id(cost_df)
    return cost_df


def init_single_prescription_df(start_date, end_date, hdf):
    #prescription_idxs = []
    country_names = []
    region_names = []
    dates = []

    #for prescription_idx in range(NUM_PRESCRIPTIONS):
    for country_name in hdf['CountryName'].unique():
        cdf = hdf[hdf['CountryName'] == country_name]
        for region_name in cdf['RegionName'].unique():
            for date in pd.date_range(start_date, end_date):
                #prescription_idxs.append(prescription_idx)
                country_names.append(country_name)
                region_names.append(region_name)
                dates.append(date)

    single_prescription_df = pd.DataFrame({
        'PrescriptionIndex': 1,
        'CountryName': country_names,
        'RegionName': region_names,
        'Date': dates})

    single_prescription_df = pd.concat(
        [hdf[hdf['Date'] < start_date], single_prescription_df])
    single_prescription_df = add_geo_id(single_prescription_df)

    return single_prescription_df


def add_plan_to_df(df, geo_id, one_day_nips, start_date, end_date):

    nb_days = len(df[(df['GeoID'] == geo_id) &
                     (df['Date'] >= start_date) &
                     (df['Date'] <= end_date)])

    df.loc[(df['GeoID'] == geo_id) &
           (df['Date'] >= start_date) &
           (df['Date'] <= end_date), IP_COLS] = np.tile(one_day_nips, (nb_days, 1))


def get_candidate_plans(new_cases: np.ndarray, expert_nips):
    nips = None
    rand_direction = 0
    if new_cases is None:
        nips = expert_nips[expert_nips['Case'] == 'Peak']
    else:
        filtered_new_case = gaussian_filter1d(new_cases, 1)
        # first order derivatives
        first_order_derivative = np.gradient(filtered_new_case, 1)[-1]
        # mean second order derivatives
        second_order_derivative = np.gradient(filtered_new_case, 2)[-1]
        # mean_so_deriv = second_order_derivative[-1]
        #print('cases','1st derivate (avg 7 days)', mean_fo_deriv, '2nd derivate (avg 7 days)', mean_so_deriv)
        # need more restriction
        if first_order_derivative > 0:
            # more restriction
            # else it works ... keep going
            nips = expert_nips[expert_nips['Case'] == 'Peak']
            rand_direction = 1 if second_order_derivative > 0 else 0
        # loss case
        else:
            nips = expert_nips[expert_nips['Case'] == 'Flat']
            rand_direction = 2 if second_order_derivative < 0 else 0

    # make a list of random plan based on expert idea
    nips_random = pd.concat([nips] * 3)

    #print(nips_random)
    plan_idx = len(nips)
    while plan_idx < len(nips_random):
        # print('rand_direction', rand_direction, 'plan_idx',plan_idx)
        for ip_name in IP_MAX_VALUES:
            ip_value = nips_random[ip_name].iloc[plan_idx]
            if rand_direction == 0:
                ip_value += np.random.randint(-1, 2)
            elif rand_direction == 1:
                ip_value += np.random.randint(0, 2)
            elif rand_direction == 2:
                ip_value += np.random.randint(-1, 1)
            #print(ip_name, np.random.randint(-1,1))
            #print(ip_value)
            if ip_value >= 0 and ip_value <= IP_MAX_VALUES[ip_name]:
                nips_random.iloc[plan_idx, nips_random.columns.get_loc(
                    ip_name)] = ip_value
        plan_idx += 1
    # print(nips_random)
    return nips_random


def fill_future(future_df: pd.DataFrame, predicted: pd.DataFrame):
    add_geo_id(predicted)
    #print()
    for index, row in predicted.iterrows():
        #print(row['GeoID'], row['Date'], row['PredictedDailyNewCases'])
        future_df.loc[(future_df['GeoID'] == row['GeoID']) & (
            future_df['Date'] == row['Date']), 'ConfirmedCases'] = row['PredictedDailyNewCases']


def prescribe(start_date_str: str,
              end_date_str: str,
              path_to_hist_file: str,
              path_to_cost_file: str,
              output_file_path) -> None:

    # Create skeleton df with one row for each prescription
    # for each geo for each day
    hdf = pd.read_csv(path_to_hist_file,
                      parse_dates=['Date'],
                      encoding="ISO-8859-1",
                      dtype={"RegionName": str},
                      error_bad_lines=True)
    #hdf = add_geo_id(hdf)
    #print(hdf)

    # read cost df
    cost_df = get_cost_df(path_to_cost_file, hdf)
    IP_MAX_ARRAY = np.array([3, 3, 2, 4, 2, 3, 2, 4, 2, 3, 2, 4])

    # read human expert nips plans
    #print(get_script_path() + '/data/human_expert_nips.csv')
    expert_nips = pd.read_csv(
        get_script_path() + '/data/human_expert_nips.csv')
    # print(expert_nips)
    # init single_prescription_df
    start_date = pd.to_datetime(start_date_str, format='%Y-%m-%d')
    end_date = pd.to_datetime(end_date_str, format='%Y-%m-%d')
    # init a empty data frame
    # prescription_df = DataFrame()

    single_prescription_df = init_single_prescription_df(
        start_date, end_date, hdf)
    #print(single_prescription_df)
    date_interval = 15

    standard_predictor = FastPredictor()
    for geo_id in single_prescription_df['GeoID'].unique():

        #if geo_id != 'United States':
        #    continue

        #if geo_id == 'Argentina':
        #    break

        # plan for current geo
        geo_df = single_prescription_df[single_prescription_df['GeoID'] == geo_id].copy(
        )

        #print(future_df)

        print('processing', geo_id)
        last_known_date = standard_predictor.get_last_known_date(geo_id)
        future_df = geo_df[geo_df['Date'] >= last_known_date].copy()
        future_df['ConfirmedCases'] = 0
        if last_known_date < start_date:
            predicted_df = standard_predictor.predict_from_df(
                last_known_date.strftime('%Y-%m-%d'), start_date_str, geo_df, None)
            fill_future(future_df, predicted_df)

        # we are only looking forward for some date_interval
        current_start_date = start_date

        case_numbers = None
        # cost df for current geo
        geo_cost = cost_df.loc[cost_df['GeoID'] ==
                               geo_id][IP_COLS].to_numpy(dtype=np.float64)

        geo_max_stringency = np.squeeze(np.matmul(IP_MAX_ARRAY, geo_cost.T))
        best_future_df = future_df
        best_next_future = future_df
        # for current geoid do a prediction
        while current_start_date < end_date:
            print(current_start_date)
            # set date for each one
            current_end_date = current_start_date + \
                timedelta(days=date_interval)
            current_start_date_str = current_start_date.strftime('%Y-%m-%d')
            current_end_date_str = current_end_date.strftime('%Y-%m-%d')
            # get some plans
            plans_df = get_candidate_plans(case_numbers, expert_nips)

            if plans_df is not None:
                best_plan_case_num = None
                best_plan_stringency = None
                best_plan_case_fd = None
                best_candidate_nips = None
                for _, plan in plans_df.iterrows():
                    candidate_nips = plan[IP_COLS].to_numpy(dtype=np.float64)
                    add_plan_to_df(geo_df, geo_id, candidate_nips,
                                   current_start_date, end_date)
                    #print(geo_df)
                    # for current geoid do a prediction
                    predicted = standard_predictor.predict_from_df(
                        current_start_date_str, current_end_date_str, geo_df, best_future_df)
                    candidate_predicted_newcases = predicted['PredictedDailyNewCases'].to_numpy(
                        dtype=np.float64)
                    candidate_nips_stringency = np.squeeze(
                        np.matmul(candidate_nips, geo_cost.T))
                    candidate_predicted_newcases_sum = np.squeeze(
                        candidate_predicted_newcases.sum())
                    #print(candidate_nips, candidate_predicted_newcases, candidate_predicted_newcases_sum, candidate_nips_stringency)
                    # print('candidate_nips_stringency', candidate_nips_stringency, 'candidate_predicted_newcases_sum', candidate_predicted_newcases_sum )
                    # this is a better plan
                    #print(candidate_nips,, candidate_predicted_newcases_sum , best_plan_stringency , candidate_nips_stringency)
                    if (best_plan_case_num is None and best_plan_stringency is None) or\
                            (best_plan_case_num > candidate_predicted_newcases_sum and best_plan_stringency > candidate_nips_stringency):
                        # this is dominating
                        best_candidate_nips = candidate_nips
                        best_plan_case_num = candidate_predicted_newcases_sum
                        best_plan_stringency = candidate_nips_stringency
                        best_plan_case_fd = get_filtered_first_order_derivative(
                            candidate_predicted_newcases)[-1]
                        best_next_future = predicted
                        case_numbers = candidate_predicted_newcases
                    else:
                        # no dominating but ... let see if new one is any better
                        if best_plan_case_num > candidate_predicted_newcases_sum or best_plan_stringency > candidate_nips_stringency:
                            # let us figue out which one is more "cost effective",
                            # NOTE THIS IS Siyang's Magic
                            # if np.log(best_plan_case_num + 1)  +  np.log(best_plan_stringency + 1) > \
                            #     np.log(candidate_predicted_newcases_sum  + 1) +  np.log(candidate_nips_stringency + 1):
                            candidate_fd = get_filtered_first_order_derivative(
                                candidate_predicted_newcases)[-1]
                            if best_plan_case_fd + best_plan_stringency / geo_max_stringency > \
                               candidate_fd + candidate_nips_stringency / geo_max_stringency:
                                best_candidate_nips = candidate_nips
                                best_plan_case_fd = candidate_fd
                                best_next_future = predicted
                                case_numbers = candidate_predicted_newcases

                # add nips to ouput frame
                fill_future(best_future_df, best_next_future)
                add_plan_to_df(single_prescription_df, geo_id,
                               best_candidate_nips, current_start_date, end_date)

            else:
                predicted = standard_predictor.predict_from_df(
                    current_start_date_str, current_end_date_str, geo_df)
                predicted_newcases = predicted['PredictedDailyNewCases'].to_numpy(
                )
                case_numbers = predicted_newcases
                fill_future(best_future_df, predicted)
            #print(predicted)
            current_start_date = current_end_date + timedelta(days=1)

    # add PrescriptionIndex
    single_prescription_df = single_prescription_df.drop('GeoID', axis=1)
    single_prescription_df['PrescriptionIndex'] = 1

    os.makedirs(os.path.dirname(output_file_path), exist_ok=True)

    # Save to a csv file
    single_prescription_df[single_prescription_df['Date']
                           >= start_date].to_csv(output_file_path, index=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--start_date",
                        dest="start_date",
                        type=str,
                        required=True,
                        help="Start date from which to prescribe, included, as YYYY-MM-DD."
                             "For example 2020-08-01")
    parser.add_argument("-e", "--end_date",
                        dest="end_date",
                        type=str,
                        required=True,
                        help="End date for the last prescription, included, as YYYY-MM-DD."
                             "For example 2020-08-31")
    parser.add_argument("-ip", "--interventions_past",
                        dest="prev_file",
                        type=str,
                        required=True,
                        help="The path to a .csv file of previous intervention plans")
    parser.add_argument("-c", "--intervention_costs",
                        dest="cost_file",
                        type=str,
                        required=True,
                        help="Path to a .csv file containing the cost of each IP for each geo")
    parser.add_argument("-o", "--output_file",
                        dest="output_file",
                        type=str,
                        required=True,
                        help="The path to an intervention plan .csv file")
    args = parser.parse_args()
    print(
        f"Generating prescriptions from {args.start_date} to {args.end_date}...")
    prescribe(args.start_date, args.end_date, args.prev_file,
              args.cost_file, args.output_file)
    print("Done!")
