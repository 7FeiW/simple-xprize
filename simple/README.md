## Example: Random prescriptor

This prescriptor prescribes random valid IPs for each day for each geo.

Example usage:
```
python prescribe.py -s 2020-08-01 -e 2020-08-31 -ip ../covid_xprize/validation/data/2020-09-30_historical_ip.csv -c ../covid_xprize/validation/data/uniform_random_costs.csv -o prescriptions/test.csv
```

```
python prescribe.py -s 2020-08-01 -e 2020-08-31 -ip ../covid_xprize/validation/data/future_ip.csv -c ../covid_xprize/validation/data/uniform_random_costs.csv -o prescriptions/test.csv
```

```
python prescribe.py -s 2021-01-01 -e 2021-01-31 -ip ./ips/prescriptions/all_2020_ips.csv -c ./ip_costs/uniform_random_costs.csv -o prescriptions/all_2021q1_test_task.csv.csv
```
