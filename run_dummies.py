import os
import sys
import argparse
import numpy as np
from numpy.core.fromnumeric import nonzero
import pandas as pd
from datetime import timedelta
from scipy.ndimage import gaussian_filter1d
import concurrent.futures
import multiprocessing

from covid_xprize.standard_predictor.xprize_predictor import XPrizePredictor
from covid_xprize.scoring.prescriptor_scoring import weight_prescriptions_by_cost
NUM_PRESCRIPTIONS = 10

IP_MAX_VALUES = {
    'C1_School closing': 3,
    'C2_Workplace closing': 3,
    'C3_Cancel public events': 2,
    'C4_Restrictions on gatherings': 4,
    'C5_Close public transport': 2,
    'C6_Stay at home requirements': 3,
    'C7_Restrictions on internal movement': 2,
    'C8_International travel controls': 4,
    'H1_Public information campaigns': 2,
    'H2_Testing policy': 3,
    'H3_Contact tracing': 2,
    'H6_Facial Coverings': 4
}

IP_COLS = list(IP_MAX_VALUES.keys())

IP_MAX_ARRAY = np.array([3, 3, 2, 4, 2, 3, 2, 4, 2, 3, 2, 4])
IP_MIN_ARRAY = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

ALL_GEO_IDS = ["Aruba", "Afghanistan", "Angola", "Albania", "Andorra", "United Arab Emirates", "Argentina", "Australia", "Austria",
               "Azerbaijan", "Burundi", "Belgium", "Benin", "Burkina Faso", "Bangladesh", "Bulgaria", "Bahrain", "Bahamas", "Bosnia and Herzegovina",
               "Belarus", "Belize", "Bermuda", "Bolivia", "Brazil/Acre", "Brazil/Alagoas", "Brazil/Amazonas", "Brazil/Amapa", "Brazil/Bahia", "Brazil/Ceara",
               "Brazil/Distrito Federal", "Brazil/Espirito Santo", "Brazil/Goias", "Brazil/Maranhao", "Brazil/Minas Gerais", "Brazil/Mato Grosso do Sul",
               "Brazil/Mato Grosso", "Brazil/Para", "Brazil/Paraiba", "Brazil/Pernambuco", "Brazil/Piaui", "Brazil/Parana", "Brazil/Rio de Janeiro",
               "Brazil/Rio Grande do Norte", "Brazil/Rondonia", "Brazil/Roraima", "Brazil/Rio Grande do Sul", "Brazil/Santa Catarina", "Brazil/Sergipe",
               "Brazil/Sao Paulo", "Brazil/Tocantins", "Brazil", "Barbados", "Brunei", "Bhutan", "Botswana", "Central African Republic", "Canada/Alberta",
               "Canada/British Columbia", "Canada/Manitoba", "Canada/New Brunswick", "Canada/Newfoundland and Labrador", "Canada/Nova Scotia",
               "Canada/Northwest Territories", "Canada/Nunavut", "Canada/Ontario", "Canada/Prince Edward Island", "Canada/Quebec",
               "Canada/Saskatchewan", "Canada/Yukon", "Canada", "Switzerland", "Chile", "China", "Cote d'Ivoire", "Cameroon", "Democratic Republic of Congo",
               "Congo", "Colombia", "Comoros", "Cape Verde", "Costa Rica", "Cuba", "Cyprus", "Czech Republic", "Germany", "Djibouti", "Dominica", "Denmark",
               "Dominican Republic", "Algeria", "Ecuador", "Egypt", "Eritrea", "Spain", "Estonia", "Ethiopia", "Finland", "Fiji", "France", "Faeroe Islands",
               "Gabon", "United Kingdom", "United Kingdom/England", "United Kingdom/Northern Ireland", "United Kingdom/Scotland", "United Kingdom/Wales",
               "Georgia", "Ghana", "Guinea", "Gambia", "Greece", "Greenland", "Guatemala", "Guam", "Guyana", "Hong Kong", "Honduras", "Croatia", "Haiti", "Hungary",
               "Indonesia", "India", "Ireland", "Iran", "Iraq", "Iceland", "Israel", "Italy", "Jamaica", "Jordan", "Japan", "Kazakhstan", "Kenya", "Kyrgyz Republic",
               "Cambodia", "Kiribati", "South Korea", "Kuwait", "Laos", "Lebanon", "Liberia", "Libya", "Liechtenstein", "Sri Lanka", "Lesotho", "Lithuania", "Luxembourg",
               "Latvia", "Macao", "Morocco", "Monaco", "Moldova", "Madagascar", "Mexico", "Mali", "Malta", "Myanmar", "Mongolia", "Mozambique", "Mauritania", "Mauritius",
               "Malawi", "Malaysia", "Namibia", "Niger", "Nigeria", "Nicaragua", "Netherlands", "Norway", "Nepal", "New Zealand", "Oman", "Pakistan", "Panama", "Peru",
               "Philippines", "Papua New Guinea", "Poland", "Puerto Rico", "Portugal", "Paraguay", "Palestine", "Qatar", "Kosovo", "Romania", "Russia", "Rwanda",
               "Saudi Arabia", "Sudan", "Senegal", "Singapore", "Solomon Islands", "Sierra Leone", "El Salvador", "San Marino", "Somalia", "Serbia", "South Sudan",
               "Suriname", "Slovak Republic", "Slovenia", "Sweden", "Eswatini", "Seychelles", "Syria", "Chad", "Togo", "Thailand", "Tajikistan", "Turkmenistan",
               "Timor-Leste", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Taiwan", "Tanzania", "Uganda", "Ukraine", "Uruguay", "United States", "United States/Alaska", "United States/Alabama", "United States/Arkansas", "United States/Arizona", "United States/California", "United States/Colorado", "United States/Connecticut", "United States/Washington DC", "United States/Delaware", "United States/Florida", "United States/Georgia", "United States/Hawaii", "United States/Iowa", "United States/Idaho", "United States/Illinois", "United States/Indiana", "United States/Kansas", "United States/Kentucky", "United States/Louisiana", "United States/Massachusetts", "United States/Maryland", "United States/Maine", "United States/Michigan", "United States/Minnesota", "United States/Missouri", "United States/Mississippi", "United States/Montana", "United States/North Carolina", "United States/North Dakota", "United States/Nebraska", "United States/New Hampshire", "United States/New Jersey", "United States/New Mexico", "United States/Nevada", "United States/New York", "United States/Ohio", "United States/Oklahoma", "United States/Oregon", "United States/Pennsylvania", "United States/Rhode Island", "United States/South Carolina", "United States/South Dakota", "United States/Tennessee", "United States/Texas", "United States/Utah", "United States/Virginia", "United States/Vermont", "United States/Washington", "United States/Wisconsin", "United States/West Virginia", "United States/Wyoming", "Uzbekistan", "Venezuela", "United States Virgin Islands", "Vietnam", "Vanuatu", "Yemen", "South Africa", "Zambia", "Zimbabwe"]

def get_prescribe_plan(start_date_str: str,
              end_date_str: str, type:str = 'min') -> None:

    start_date = pd.to_datetime(start_date_str, format='%Y-%m-%d')
    end_date = pd.to_datetime(end_date_str, format='%Y-%m-%d')
    country_names = []
    region_names = []
    geo_ids = []
    dates = []

    for goe_id in ALL_GEO_IDS:
        #prescription_df["GeoID"]  = goe_id
        country_name = goe_id
        region_name = ''
        if '/' in goe_id:
            country_name, region_name = goe_id.split('/')

        for date in pd.date_range(start_date, end_date):
            country_names.append(country_name)
            region_names.append(region_name)
            geo_ids.append(goe_id)
            dates.append(date)

    prescription_df = pd.DataFrame({
        'CountryName': country_names,
        'RegionName': region_names,
        'GeoID': geo_ids,
        'Date': dates})

    # Fill df with all zeros
    if type == 'min':
        for npi_col in IP_COLS:
            prescription_df[npi_col] = 0
    if type == 'max':
        for npi_col in IP_MAX_VALUES:
            prescription_df[npi_col] = IP_MAX_VALUES[npi_col]
    return prescription_df


def prescribe_worker(start_date, end_date, type_str):
    standard_predictor = XPrizePredictor()
    start_date_str = start_date.strftime('%Y-%m-%d')
    end_date_str = end_date.strftime('%Y-%m-%d')

    # for current geoid do a prediction

    npis_df = get_prescribe_plan(start_date, end_date, type_str)
    #print(type(start_date_str),type(end_date_str))
    predicted = standard_predictor.predict_from_df(
        start_date_str, end_date_str, npis_df)
    predicted.to_csv(
        './dummy_data/{}_npis_{}_{}.csv'.format(type_str, start_date_str, end_date_str), index=False)

    return 'finished {} {} with {} npis'.format(start_date_str, end_date_str, type_str)


def prescribe(start_date_str: str,
              end_date_str: str, type_str) -> None:

    date_interval = 14

    start_date = pd.to_datetime(start_date_str, format='%Y-%m-%d')
    end_date = pd.to_datetime(end_date_str, format='%Y-%m-%d')

    # populate date
    current_start_date = start_date
    current_end_date = start_date + timedelta(days=date_interval)
    start_dates = [start_date]
    end_dates = [current_end_date]
    while current_end_date < end_date:
        current_start_date += timedelta(days=1)
        current_end_date += timedelta(days=1)
        start_dates.append(current_start_date)
        end_dates.append(current_end_date)
        #current_start_date + timedelta(days = date_interval)
    type_strs = [type_str] * len(start_dates)
    #print(len(type_strs), len(start_dates))
    with concurrent.futures.ProcessPoolExecutor() as executor:
        for msg in executor.map(prescribe_worker, start_dates, end_dates, type_strs):
            print(msg)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--start_date",
                        dest="start_date",
                        type=str,
                        required=True,
                        help="Start date from which to prescribe, included, as YYYY-MM-DD."
                             "For example 2020-08-01")
    parser.add_argument("-e", "--end_date",
                        dest="end_date",
                        type=str,
                        required=True,
                        help="End date for the last prescription, included, as YYYY-MM-DD."
                             "For example 2020-08-31")
    parser.add_argument("-t", "--type",
                        dest="type_str",
                        type=str,
                        required=True,
                        help="min or max")

    args = parser.parse_args()
    print(
        f"Generating prescriptions from {args.start_date} to {args.end_date} with {args.type_str} nips")
    prescribe(args.start_date, args.end_date, args.type_str)
    print("Done!")
